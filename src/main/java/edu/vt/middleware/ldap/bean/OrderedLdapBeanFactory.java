/*
  $Id: OrderedLdapBeanFactory.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.bean;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <code>OrderedLdapBeanFactory</code> provides an ldap bean factory that
 * produces ordered ldap beans.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class OrderedLdapBeanFactory implements LdapBeanFactory
{


  /** {@inheritDoc} */
  public LdapResult newLdapResult()
  {
    return new OrderedLdapResult();
  }


  /** {@inheritDoc} */
  public LdapEntry newLdapEntry()
  {
    return new OrderedLdapEntry();
  }


  /** {@inheritDoc} */
  public LdapAttributes newLdapAttributes()
  {
    return new OrderedLdapAttributes();
  }


  /** {@inheritDoc} */
  public LdapAttribute newLdapAttribute()
  {
    return new OrderedLdapAttribute();
  }


  /**
   * <code>OrderedLdapResult</code> represents a collection of ldap entries that
   * are ordered by insertion.
   */
  protected class OrderedLdapResult
    extends AbstractLdapResult<LinkedHashMap<String, LdapEntry>>
  {


    /** Default constructor. */
    public OrderedLdapResult()
    {
      super(OrderedLdapBeanFactory.this);
      this.entries = new LinkedHashMap<String, LdapEntry>();
    }
  }


  /** <code>OrderedLdapEntry</code> represents a single ldap entry. */
  protected class OrderedLdapEntry extends AbstractLdapEntry
  {


    /** Default constructor. */
    public OrderedLdapEntry()
    {
      super(OrderedLdapBeanFactory.this);
      this.ldapAttributes = new OrderedLdapAttributes();
    }
  }


  /**
   * <code>OrderedLdapAttributes</code> represents a collection of ldap
   * attribute that are ordered by insertion.
   */
  protected class OrderedLdapAttributes
    extends AbstractLdapAttributes<LinkedHashMap<String, LdapAttribute>>
  {


    /** Default constructor. */
    public OrderedLdapAttributes()
    {
      super(OrderedLdapBeanFactory.this);
      this.attributes = new LinkedHashMap<String, LdapAttribute>();
    }
  }


  /**
   * <code>OrderedLdapAttribute</code> represents a single ldap attribute whose
   * values are ordered by insertion.
   */
  protected class OrderedLdapAttribute
    extends AbstractLdapAttribute<LinkedHashSet<Object>>
  {


    /** Default constructor. */
    public OrderedLdapAttribute()
    {
      super(OrderedLdapBeanFactory.this);
      this.values = new LinkedHashSet<Object>();
    }


    /** {@inheritDoc} */
    public Set<String> getStringValues()
    {
      final Set<String> s = new LinkedHashSet<String>();
      this.convertValuesToString(s);
      return Collections.unmodifiableSet(s);
    }
  }
}
