/*
  $Id: SingletonTLSSocketFactory.java 1742 2010-11-19 15:18:06Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1742 $
  Updated: $Date: 2010-11-19 16:18:06 +0100 (Fri, 19 Nov 2010) $
*/
package edu.vt.middleware.ldap.ssl;

import java.security.GeneralSecurityException;
import javax.net.SocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * TLSSocketFactory implementation that uses a static SSLContextInitializer.
 * Useful for SSL configurations that can only retrieve the SSLSocketFactory
 * from getDefault().
 *
 * @author  Middleware Services
 * @version  $Revision: 1742 $ $Date: 2010-11-19 16:18:06 +0100 (Fri, 19 Nov 2010) $
 */
public class SingletonTLSSocketFactory extends TLSSocketFactory
{
  /** SSLContextInitializer used for initializing SSL contexts. */
  protected static SSLContextInitializer staticContextInitializer;


  /** {@inheritDoc} */
  public void setSSLContextInitializer(final SSLContextInitializer initializer)
  {
    if (staticContextInitializer != null) {
      final Log logger = LogFactory.getLog(SingletonTLSSocketFactory.class);
      if (logger.isWarnEnabled()) {
        logger.warn("SSLContextInitializer is being overridden");
      }
    }
    staticContextInitializer = initializer;
  }


  /** {@inheritDoc} */
  public void initialize()
    throws GeneralSecurityException
  {
    super.setSSLContextInitializer(staticContextInitializer);
    super.initialize();
  }


  /**
   * This returns the default SSL socket factory.
   *
   * @return  <code>SocketFactory</code>
   */
  public static SocketFactory getDefault()
  {
    final SingletonTLSSocketFactory sf = new SingletonTLSSocketFactory();
    try {
      sf.initialize();
    } catch (GeneralSecurityException e) {
      final Log logger = LogFactory.getLog(SingletonTLSSocketFactory.class);
      if (logger.isErrorEnabled()) {
        logger.error("Error initializing socket factory", e);
      }
    }
    return sf;
  }
}
