/*
  $Id: CertificateHostnameVerifier.java 2231 2012-02-02 15:46:27Z dfisher $

  Copyright (C) 2003-2012 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 2231 $
  Updated: $Date: 2012-02-02 16:46:27 +0100 (Thu, 02 Feb 2012) $
*/
package edu.vt.middleware.ldap.ssl;

import java.security.cert.X509Certificate;

/**
 * Interface for verifying a hostname matching a certificate.
 *
 * @author  Middleware Services
 * @version  $Revision: 2231 $
 */
public interface CertificateHostnameVerifier
{


  /**
   * Verify the supplied hostname matches the supplied certificate.
   *
   * @param  hostname  to verify
   * @param  cert  to verify hostname against
   *
   * @return  whether hostname is valid for the supplied certificate
   */
  boolean verify(final String hostname, final X509Certificate cert);
}
