/*
  $Id: BindAuthenticationHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth.handler;

import javax.naming.NamingException;
import edu.vt.middleware.ldap.auth.AuthenticatorConfig;
import edu.vt.middleware.ldap.handler.ConnectionHandler;

/**
 * <code>BindAuthenticationHandler</code> provides an LDAP authentication
 * implementation that leverages the LDAP bind operation.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public class BindAuthenticationHandler extends AbstractAuthenticationHandler
{


  /** Default constructor. */
  public BindAuthenticationHandler() {}


  /**
   * Creates a new <code>BindAuthenticationHandler</code> with the supplied
   * authenticator config.
   *
   * @param  ac  authenticator config
   */
  public BindAuthenticationHandler(final AuthenticatorConfig ac)
  {
    this.setAuthenticatorConfig(ac);
  }


  /** {@inheritDoc} */
  public void authenticate(
    final ConnectionHandler ch,
    final AuthenticationCriteria ac)
    throws NamingException
  {
    ch.connect(ac.getDn(), ac.getCredential());
  }


  /** {@inheritDoc} */
  public BindAuthenticationHandler newInstance()
  {
    return new BindAuthenticationHandler(this.config);
  }
}
