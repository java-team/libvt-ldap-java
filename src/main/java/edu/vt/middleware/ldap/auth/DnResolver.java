/*
  $Id: DnResolver.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth;

import javax.naming.NamingException;

/**
 * <code>DnResolver</code> provides an interface for finding LDAP DNs.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public interface DnResolver
{

  /**
   * Attempts to find the LDAP DN for the supplied user.
   *
   * @param  user  <code>String</code> to find dn for
   *
   * @return  <code>String</code> - user's dn
   *
   * @throws  NamingException  if an LDAP error occurs
   */
  String resolve(String user)
    throws NamingException;


  /**
   * Returns the authenticator config.
   *
   * @return  authenticator configuration
   */
  AuthenticatorConfig getAuthenticatorConfig();


  /**
   * Sets the authenticator config.
   *
   * @param  config  authenticator configuration
   */
  void setAuthenticatorConfig(AuthenticatorConfig config);


  /** This will close any resources associated with this resolver. */
  void close();
}
