/*
  $Id: AuthenticationResultHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth.handler;

/**
 * AuthenticationResultHandler provides post processing of authentication
 * results.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public interface AuthenticationResultHandler
{


  /**
   * Process the results from an ldap authentication.
   *
   * @param  ac  <code>AuthenticationCriteria</code> used to perform the
   * authentication
   * @param  success  <code>boolean</code> whether the authentication succeeded
   */
  void process(AuthenticationCriteria ac, boolean success);
}
