/*
  $Id: AttributeHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.handler;

import javax.naming.directory.Attribute;

/**
 * AttributeHandler provides post search processing of an ldap attribute.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public interface AttributeHandler extends ResultHandler<Attribute, Attribute> {}
