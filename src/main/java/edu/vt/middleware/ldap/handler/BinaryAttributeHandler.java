/*
  $Id: BinaryAttributeHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.handler;

import edu.vt.middleware.ldap.LdapUtil;

/**
 * <code>BinaryAttributeHandler</code> ensures that any attribute that contains
 * a value of type byte[] is base64 encoded.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class BinaryAttributeHandler extends CopyAttributeHandler
{


  /**
   * This base64 encodes the supplied value if it is of type byte[].
   *
   * @param  sc  <code>SearchCriteria</code> used to find enumeration
   * @param  value  <code>Object</code> to process
   *
   * @return  <code>Object</code>
   */
  protected Object processValue(final SearchCriteria sc, final Object value)
  {
    if (value instanceof byte[]) {
      return LdapUtil.base64Encode((byte[]) value);
    } else {
      return value;
    }
  }
}
