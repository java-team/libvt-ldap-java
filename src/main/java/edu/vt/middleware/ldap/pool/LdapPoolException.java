/*
  $Id: LdapPoolException.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.pool;

/**
 * <code>LdapPoolException</code> is the base exception thrown when a pool
 * operation fails.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public class LdapPoolException extends Exception
{

  /** serialVersionUID. */
  private static final long serialVersionUID = 4077412841480524865L;


  /**
   * This creates a new <code>LdapPoolException</code> with the supplied <code>
   * String</code>.
   *
   * @param  msg  <code>String</code>
   */
  public LdapPoolException(final String msg)
  {
    super(msg);
  }


  /**
   * This creates a new <code>LdapPoolException</code> with the supplied <code>
   * Exception</code>.
   *
   * @param  e  <code>Exception</code>
   */
  public LdapPoolException(final Exception e)
  {
    super(e);
  }


  /**
   * This creates a new <code>LdapPoolException</code> with the supplied <code>
   * String</code> and <code>Exception</code>.
   *
   * @param  msg  <code>String</code>
   * @param  e  <code>Exception</code>
   */
  public LdapPoolException(final String msg, final Exception e)
  {
    super(msg, e);
  }
}
