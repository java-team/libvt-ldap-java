/*
  $Id: LdapConnTest.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Parameters;

/**
 * Sleeps at the end of all tests and check open connections.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public class LdapConnTest
{


  /**
   * @param  host  to check for connections with.
   * @param  sleepTime  time to sleep for.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters({ "ldapHost", "sleepTime" })
  @AfterSuite(groups = {"conntest"})
  public void sleep(final String host, final int sleepTime)
    throws Exception
  {
    Thread.sleep(sleepTime);

    /*
     * -- expected open connections --
     * LdapTest: 1
     * LdapCliTest:0
     * AuthenticatorTest: 2
     * AuthenticatorCliTest: 0
     * LdapResultTest: 0
     * LdapLoginModuleTest: 0
     * SessionManagerTest: 1
     * SearchServletTest: 6
     * AttributeServletTest: 3
     */
    final int openConns = TestUtil.countOpenConnections(host);
    AssertJUnit.assertEquals(13, openConns);
  }
}
