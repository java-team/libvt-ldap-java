/*
  $Id: AnyHostnameVerifier.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * <code>AnyHostnameVerifier</code> returns true for any host.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class AnyHostnameVerifier implements HostnameVerifier
{


  /** {@inheritDoc} */
  public boolean verify(final String hostname, final SSLSession seession)
  {
    return true;
  }


  /**
   * Dummy getter method.
   *
   * @return  'foo'
   */
  public String getFoo()
  {
    return "foo";
  }


  /**
   * Dummy setter method. Noop.
   *
   * @param  s  <code>String</code>
   */
  public void setFoo(final String s) {}


  /**
   * Dummy getter method.
   *
   * @return  true
   */
  public boolean getBar()
  {
    return true;
  }


  /**
   * Dummy setter method. Noop.
   *
   * @param  b  <code>boolean</code>
   */
  public void setBar(final boolean b) {}
}
