/*
  $Id: TestAuthorizationHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth.handler;

import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import edu.vt.middleware.ldap.auth.AuthorizationException;

/**
 * <code>TestAuthenticationResultHandler</code>.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class TestAuthorizationHandler implements AuthorizationHandler
{

  /** results. */
  private List<String> results = new ArrayList<String>();

  /** whether process should succeed. */
  private boolean succeed;


  /** {@inheritDoc} */
  public void process(final AuthenticationCriteria ac, final LdapContext ctx)
    throws NamingException
  {
    if (!succeed) {
      throw new AuthorizationException("Succeed is false");
    }
    this.results.add(ac.getDn());
  }


  /**
   * Returns the authentication results.
   *
   * @return  authentication results
   */
  public List<String> getResults()
  {
    return this.results;
  }


  /**
   * Sets whether process will succeed.
   *
   * @param  b  <code>boolean</code>
   */
  public void setSucceed(final boolean b)
  {
    succeed = b;
  }
}
