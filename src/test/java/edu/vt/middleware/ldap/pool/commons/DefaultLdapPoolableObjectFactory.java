/*
  $Id: DefaultLdapPoolableObjectFactory.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.pool.commons;

import edu.vt.middleware.ldap.Ldap;
import edu.vt.middleware.ldap.pool.DefaultLdapFactory;
import org.apache.commons.pool.PoolableObjectFactory;

/**
 * <code>DefaultLdapPoolableObjectFactory</code> provides a implementation of a
 * commons pooling <code>PoolableObjectFactory</code> for testing.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class DefaultLdapPoolableObjectFactory extends DefaultLdapFactory
  implements PoolableObjectFactory
{

  /** {@inheritDoc} */
  public void activateObject(final Object obj)
  {
    this.activate((Ldap) obj);
  }


  /** {@inheritDoc} */
  public void destroyObject(final Object obj)
  {
    this.destroy((Ldap) obj);
  }


  /** {@inheritDoc} */
  public Object makeObject()
  {
    return this.create();
  }


  /** {@inheritDoc} */
  public void passivateObject(final Object obj)
  {
    this.passivate((Ldap) obj);
  }


  /** {@inheritDoc} */
  public boolean validateObject(final Object obj)
  {
    return this.validate((Ldap) obj);
  }
}
